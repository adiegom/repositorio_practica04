package clases;

/**
 * 
 * Esta clase pretende albergar los diferentes metodos usados en el programa
 * @author Hello there
 *
 */

public class Composiciones {
	private Unidades[] unidades;

	public Composiciones(int maximoComposiciones) {
		this.unidades = new Unidades[maximoComposiciones];
	}
	
	/**
	 * 
	 * @param comandante. este parametro te permite a�adir un comandante
	 * @param nombre. este parametro te permite a�adir un nombre de unidad
	 * @param tipo. este parametro te permite a�adir un tipo de unidad
	 * @param faccion. este parametro te permite a�adir una faccion
	 * @param numeroEfectivos.  este parametro te permite a�adir la cantidad de efectivos
	 * 
	 */

	public void altaUnidad(String comandante, String nombre, String tipo, String faccion, int numeroEfectivos) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] == null) {
				unidades[i] = new Unidades(comandante);
				unidades[i].setNombre(nombre);
				unidades[i].setTipo(tipo);
				unidades[i].setFaccion(faccion);
				unidades[i].setNumeroEfectivos(numeroEfectivos);
				break;
			}
		}
	}
	
	/**
	 * 
	 * @param comandante
	 * @return devuelve el nombre de la unidad
	 */

	public Unidades buscarUnidad(String comandante) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i].getComandante().equalsIgnoreCase(comandante)) {
				return unidades[i];
			}
		}
		return null;
	}
	
	/**
	 * Elimina la unidad elegida mediante su comandante
	 * @param comandante
	 */

	public void eliminarUnidad(String comandante) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i].getComandante().equalsIgnoreCase(comandante)) {
				unidades[i] = null;
			}
		}
	}

	/**
	 * Elimina todo todito todo
	 * Be careful
	 * 
	 */
	
	public void eliminarTodo() {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				
					unidades[i] = null;
				}
			}
		}
	
	/**
	 * 
	 * Muestra todas los datos
	 * 
	 */

	public void listaUnidades() {
		for (int i = 0; i < unidades.length; i++) {
			System.out.println(unidades[i]);
		}
	}
	
	/**
	 * 
	 * Muestra los datos de la faccion elegida
	 * @param faccion
	 */

	public void listarUnidadesPorFaccion(String faccion) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				if (unidades[i].getComandante().equalsIgnoreCase(faccion)) {
					System.out.println(unidades[i]);
				}
			}
		}
	}
	
	/**
	 * 
	 * Cambia al comandante
	 * @param comandante
	 * @param nuevoComandante
	 */
	
	public void cambiarComandante(String comandante, String nuevoComandante) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				if (unidades[i].getComandante().equalsIgnoreCase(comandante )) {
					unidades[i].setComandante(nuevoComandante);
				}
			}
		}
	}
	
	/**
	 * cambia al nombre de la unidad
	 * @param comandante
	 * @param nuevoNombre
	 */
	
	public void cambiarNombre(String comandante, String nuevoNombre) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				if (unidades[i].getComandante().equalsIgnoreCase(comandante)) {
					unidades[i].setNombre(nuevoNombre);
				}
			}
		}
	}
	
	/**
	 * 
	 * camnia el tipo de unidad
	 * @param comandante
	 * @param nuevoTipo
	 */
	
	public void cambiarTipo(String comandante, String nuevoTipo) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				if (unidades[i].getComandante().equalsIgnoreCase(comandante)) {
					unidades[i].setTipo(nuevoTipo);
				}
			}
		}
	}
	
	/**
	 * 
	 * cambia la faccion
	 * @param comandante
	 * @param nuevoFaccion
	 */
	
	public void cambiarFaccion(String comandante, String nuevoFaccion) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				if (unidades[i].getComandante().equalsIgnoreCase(comandante)) {
					unidades[i].setFaccion(nuevoFaccion);
				}
			}
		}
	}
	
	/**
	 * 
	 * cambia el numero de efectivos
	 * @param comandante
	 * @param nuevoNumeroEfectivos
	 */
	
	public void cambiarNumeroEfectivos(String comandante, int nuevoNumeroEfectivos) {
		for (int i = 0; i < unidades.length; i++) {
			if (unidades[i] != null) {
				if (unidades[i].getComandante().equals(comandante)) {
					unidades[i].setNumeroEfectivos(nuevoNumeroEfectivos);
				}
			}
		}
	}
	

}
