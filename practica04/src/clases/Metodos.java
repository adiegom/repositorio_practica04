package clases;

import java.util.Scanner;
import clases.Composiciones;

/**
 * esta es la clase de los metodos principales
 * @author Hello there
 *
 */

public class Metodos {
	/**
	 * este metodo se usa para saludar
	 */
	static public void saludo() {
		System.out.println("��BIENVENID ...!! ��OH NO!!  ��TU OTRA VEZ??");
		System.out.println("�Que necesitas ahora? (Recuerda que el menu es numerico)");
		System.out.println();
	}
	
	/**
	 * 
	 * este es el metodo del menu y submenu, donde se elige
	 * lo que se quiere hacer
	 * 
	 */
	static public void toLoGrosso() {
		
		Scanner input = new Scanner(System.in);
		Composiciones comp1 = new Composiciones(6);
		boolean dW = false;
		String comandante, nombre, tipo, faccion;
		int numeroEfectivos;
		String eliminacionTotal;
		
	do {
		System.out.println("Elija lo que desea hacer: ");
		System.out.println("*-------------------------------*");
		System.out.println("1: Dar de alta              *");
		System.out.println("2: Buscar                   *");
		System.out.println("3: Eliminar                 *");
		System.out.println("4: Eliminar todo            *");
		System.out.println("5: Listar                   *");
		System.out.println("6: Listar por faccion       *");
		System.out.println("7: Modificar datos          *");
		System.out.println("*-------------------------------*");

		int menu = input.nextInt();
		input.nextLine();

		switch (menu) {

		case 1:
			System.out.println("Introduzca el nombre del comandante de las fuerzas militarosas: ");
			comandante = input.nextLine();

			System.out.println("Introduzca el nombre de la unidad militarosa: ");
			nombre = input.nextLine();

			System.out.println("Introduzca el tipo de la unidad militarosa: ");
			tipo = input.nextLine();

			System.out.println("Introduzca la faccion de la unidad militarosa: ");
			faccion = input.nextLine();

			System.out.println("Introduzca el numero de efectivos de las fuerzas militarosas de esa unidad: ");
			numeroEfectivos = input.nextInt();

			comp1.altaUnidad(comandante, nombre, tipo, faccion, numeroEfectivos);

			break;
		case 2:
			System.out.println("�Busque unidad mediante su Comandante?");
			comandante = input.nextLine();
			System.out.println(comp1.buscarUnidad(comandante));
			break;
		case 3:
			System.out.println("�Que unidad desea eliminar?");
			comandante = input.nextLine();
			comp1.eliminarUnidad(comandante);
			break;
		case 4:
			System.out.println("�Esta a punto de eliminar todo, esta seguro que desea hacerlo?");
			do {
				eliminacionTotal = input.nextLine();
				if (eliminacionTotal.equalsIgnoreCase("s")) {
					comp1.eliminarTodo();
				} else {
					break;
				}

			} while (dW == true);
			break;
		case 5:
			System.out.println("La actual lista esta compuesta por: ");
			comp1.listaUnidades();
			break;
		case 6:
			System.out.println("Que unidad desea buscar segun su Faccion?");
			faccion = input.nextLine();
			comp1.listarUnidadesPorFaccion(faccion);
			break;

		case 7:

			do {
				System.out.println("�Que dato desea modificar? ");
				System.out.println("*-----------------------------------*");
				System.out.println("1: Modificar Comandante          *");
				System.out.println("2: Modificar Nombre              *");
				System.out.println("3: Modificar Tipo                *");
				System.out.println("4: Modificar Faccion             *");
				System.out.println("5: Modificar numero de Efectivos *");
				System.out.println("*-----------------------------------*");

				menu = input.nextInt();
				input.nextLine();

				switch (menu) {

				case 1:
					do {
						System.out.println("Introduce el comandante que desea modificar");
						comandante = input.nextLine();
						if (comp1.buscarUnidad(comandante) == null) {
							System.out.println("El comandante no coincide con los datos registrados");
							System.out.println("Introduzca uno existente");
							dW = true;
						} else {
							dW = false;

						}

					} while (dW);
					System.out.println("Ha seleccionado modificar el comandante");
					String nuevoComandante = input.nextLine();
					comp1.cambiarComandante(comandante, nuevoComandante);
					comp1.listaUnidades();
					break;

				case 2:
					do {
						System.out.println("Introduce el comandante que desea modificar");
						comandante = input.nextLine();
						if (comp1.buscarUnidad(comandante) == null) {
							System.out.println("El comandante no coincide con los datos registrados");
							System.out.println("Introduzca uno existente");
							dW = true;
						} else {
							dW = false;

						}

					} while (dW);

					System.out.println("Ha seleccionado modificar el nombre");
					String nuevoNombre = input.nextLine();
					comp1.cambiarNombre(comandante, nuevoNombre);
					comp1.listaUnidades();
					break;

				case 3:
					do {
						System.out.println("Introduce el comandante que desea modificar");
						comandante = input.nextLine();
						if (comp1.buscarUnidad(comandante) == null) {
							System.out.println("El comandante no coincide con los datos registrados");
							System.out.println("Introduzca uno existente");
							dW = true;
						} else {
							dW = false;

						}

					} while (dW);

					System.out.println("Ha seleccionado modificar el tipo");
					String nuevoTipo = input.nextLine();
					comp1.cambiarTipo(comandante, nuevoTipo);
					comp1.listaUnidades();
					break;

				case 4:
					do {
						System.out.println("Introduce el comandante que desea modificar");
						comandante = input.nextLine();
						if (comp1.buscarUnidad(comandante) == null) {
							System.out.println("El comandante no coincide con los datos registrados");
							System.out.println("Introduzca uno existente");
							dW = true;
						} else {
							dW = false;

						}

					} while (dW);

					System.out.println("Ha seleccionado modificar la faccion");
					String nuevoFaccion = input.nextLine();
					comp1.cambiarFaccion(comandante, nuevoFaccion);
					comp1.listaUnidades();
					break;

				case 5:

					do {
						System.out.println("Introduce el comandante que desea modificar");
						comandante = input.nextLine();
						if (comp1.buscarUnidad(comandante) == null) {
							System.out.println("El comandante no coincide con los datos registrados");
							System.out.println("Introduzca uno existente");
							dW = true;
						} else {
							dW = false;

						}

					} while (dW);
					System.out.println("Ha seleccionado modificar el numero de efectivos");
					int nuevoNumeroEfectivos = input.nextInt();
					comp1.cambiarNumeroEfectivos(comandante, nuevoNumeroEfectivos);
					comp1.listaUnidades();
					break;

				}
			} while (dW);

			break;

		}

	} while (dW);
	input.close();
	
	}
	}



