package clases;

/**
 * 
 * Esta clase es la que alberga los setters y getters del programa
 * en cuestion.
 * @author Hello there
 *
 */

public class Unidades {
	
	private String comandante; //
	private String nombre; //
	private String tipo;	//
	private String faccion; //
	private int numeroEfectivos;//
	
	public Unidades(String nombreComandante){
		this.comandante = nombreComandante;
	}
	
	public String getComandante() {
		return comandante;
	}
	
	public void setComandante(String comandante) {
		this.comandante =comandante;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre =nombre;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFaccion() {
		return faccion;
	}
	
	public void setFaccion(String faccion) {
		this.faccion =faccion;
	}
	public int getNumeroEfectivos() {
		return numeroEfectivos;
	}
	
	public void setNumeroEfectivos(int numeroEfectivos) {
		this.numeroEfectivos =numeroEfectivos;
	}
	
	public String toString() {

		return "Comandante: " + comandante + ",/n " + "Nombre: " +  nombre + ",/n " +
		"Tipo: " + tipo + ",/n " + "Faccion: " + faccion + ",/n " + "Efectivos: " + numeroEfectivos ;
	}
	

}
